/*
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "repliesqmlwrapper.h"

RepliesQMLWrapper::RepliesQMLWrapper(const Akonadi::SocialFeedItem &postReply, const QString &postId, QObject *parent)
    : QObject(parent)
{
    m_postReply = postReply;
    m_postId = postId;
}

QString RepliesQMLWrapper::userId() const
{
    return m_postReply.userId();
}

QString RepliesQMLWrapper::userName() const
{
    return m_postReply.userName();
}

QString RepliesQMLWrapper::userAvatarUrl() const
{
    return m_postReply.avatarUrl().toString();
}

QString RepliesQMLWrapper::replyText() const
{
    return m_postReply.postText();
}

QString RepliesQMLWrapper::replyTime() const
{
//     return m_postReply.postTime();
    return QString();
}

QString RepliesQMLWrapper::replyId() const
{
    return m_postReply.postId();
}

QString RepliesQMLWrapper::postId() const
{
    return m_postId;
}
