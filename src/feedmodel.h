/*
    Social feed model class
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef FEEDMODEL_H
#define FEEDMODEL_H

#include <Akonadi/EntityTreeModel>
#include <Akonadi/Item>
#include <Akonadi/Collection>

#include <Akonadi/SocialUtils/ImageProvider>
#include <Akonadi/SocialUtils/SocialFeedItem>

class RepliesQMLWrapper;
namespace Akonadi {
    class ChangeRecorder;
}

class FeedModel : public Akonadi::EntityTreeModel
{
    Q_OBJECT
public:

    enum Role {
        ScreenNameRole = Qt::UserRole + 12000,
        AvatarRole,
        NetworkStringRole,
        StatusMessageRole,
        StatusLinkRole,
        StatusLinkTitleRole,
        StatusImageUrlRole,
        StatusImageRole,
        StatusTimeRole,
        StatusTimeStringRole,
        StatusInfoRole,
        StatusLikedRole,
        StatusRepliesRole,
        StatusIdRole,
        AkonadiIdRole
    };

    FeedModel(Akonadi::ChangeRecorder *monitor, QObject *parent = 0);
    ~FeedModel();

    QVariant data (const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant entityData(const Akonadi::Collection &collection, int column, int role = Qt::DisplayRole) const;
    int entityColumnCount(HeaderGroup headerGroup) const;

private Q_SLOTS:
    void onImageLoaded(const QString &who, const KUrl &url, const QImage &image);

private:
    QList<QObject*> wrapRepliesForQML(QList<Akonadi::SocialFeedItem> postReplies, const QString &postId) const;

    //the ImageProvider needs to be mutable because it uses non-const method from data(..) const;
    mutable Akonadi::ImageProvider m_avatarProvider;
    mutable QMultiHash<QString, QPersistentModelIndex> m_queuedImages;
    mutable QHash<QString, QImage> m_imageCache;
    mutable QHash<QString, QList<QObject*> > m_cachedReplies; //FIXME use cache/QCache to remove old stuff

};

Q_DECLARE_METATYPE(QList<QObject*>)

#endif // FEEDMODEL_H
