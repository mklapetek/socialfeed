/*
    Social feed
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef SOCIALFEED_H
#define SOCIALFEED_H

#include <QObject>

#include <Akonadi/Collection>

class CollectionQmlItem;
class KJob;
class QSortFilterProxyModel;
class FeedModel;

namespace Akonadi {
    class EntityMimeTypeFilterModel;
}

class SocialFeed : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* model READ model)
    Q_PROPERTY(QList<QObject*> collectionsList READ collectionsList NOTIFY collectionsListChanged)

public:
    SocialFeed(QObject *parent = 0);
    virtual ~SocialFeed();

    QSortFilterProxyModel* model() const;
    QList<QObject*> collectionsList() const;

    Q_INVOKABLE int smallestMaxPostLength() const;

Q_SIGNALS:
    void collectionsListChanged();
    void statusUpdated();
    void statusUpdateError(const QString &error);
    void collectionsRefreshed();

public Q_SLOTS:
    void onPostMessage(const QString &message);
    void onResfreshCollections();
    void onLikePost(const QString &akonadiId, const QString &remoteId, bool liked = true);
    void onShowPost(const QString &akonadiId, const QString &remoteId);

private Q_SLOTS:
    void onCollectionsFetched(const Akonadi::Collection::List &list);
    void onCollectionsFetchJobFinished(KJob*);
    void onItemCreateJobFinished(KJob*);

    void onItemFetechedForLiking(KJob*);
    void onItemModified(KJob*);
private:
    bool testAndSetOnlineResources(const Akonadi::Collection &collection);

    FeedModel *m_model;
    QSortFilterProxyModel *m_sortModel;
    QList<QObject*> m_collectionsList;
    QList<KJob*> m_postingJobs;
};

#endif // _SOCIALFEED_H_
