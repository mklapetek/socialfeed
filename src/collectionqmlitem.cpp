/*
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "collectionqmlitem.h"

CollectionQmlItem::CollectionQmlItem(const QString &name, const QIcon &icon, bool checked, uint maxPostLength, QObject *parent)
    : QObject (parent)
{
    m_name = name;
    m_icon = icon;
    m_checked = checked;
    m_maxPostLength = maxPostLength;
}

CollectionQmlItem::~CollectionQmlItem()
{
}

QString CollectionQmlItem::name() const
{
    return m_name;
}

void CollectionQmlItem::setName(const QString &name)
{
    m_name = name;
}

QIcon CollectionQmlItem::icon() const
{
    return m_icon;
}

void CollectionQmlItem::setIcon(const QIcon &icon)
{
    m_icon = icon;
}

bool CollectionQmlItem::isChecked() const
{
    return m_checked;
}

void CollectionQmlItem::setChecked(bool checked)
{
    m_checked = checked;
}

uint CollectionQmlItem::maxPostLength() const
{
    return m_maxPostLength;
}

void CollectionQmlItem::setMaxPostLength(uint maxPostLength)
{
    m_maxPostLength = maxPostLength;
}

Akonadi::Collection CollectionQmlItem::collection() const
{
    return m_collection;
}

void CollectionQmlItem::setCollection(const Akonadi::Collection &collection)
{
    m_collection = collection;
}
