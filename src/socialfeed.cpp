/*
    Social feed
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "socialfeed.h"
#include "feedmodel.h"
#include "collectionqmlitem.h"

#include <Akonadi/AgentManager>
#include <Akonadi/AttributeFactory>
#include <Akonadi/ChangeRecorder>
#include <Akonadi/CollectionFetchJob>
#include <Akonadi/CollectionFetchScope>
#include <Akonadi/EntityTreeModel>
#include <Akonadi/EntityDisplayAttribute>
#include <Akonadi/ItemFetchScope>
#include <Akonadi/ItemCreateJob>
#include <Akonadi/ItemFetchJob>
#include <Akonadi/ItemModifyJob>

#include <KIcon>
#include <KLocalizedString>
#include <KMessageBox>

#include <QListView>
#include <QSortFilterProxyModel>

#include <Akonadi/SocialUtils/SocialNetworkAttributes>
#include <Akonadi/SocialUtils/SocialFeedItem>

using namespace Akonadi;

SocialFeed::SocialFeed(QObject *parent)
    : QObject(parent)
{
    AttributeFactory::registerAttribute<SocialNetworkAttributes>();

    Akonadi::ItemFetchScope scope;
    scope.fetchFullPayload();

    CollectionFetchJob *job = new CollectionFetchJob(Collection::root(), CollectionFetchJob::Recursive, this);
    job->fetchScope().setContentMimeTypes(QStringList() << "text/x-vnd.akonadi.socialfeeditem");

    connect(job, SIGNAL(collectionsReceived(Akonadi::Collection::List)),
            this, SLOT(onCollectionsFetched(Akonadi::Collection::List)));
    connect(job, SIGNAL(result(KJob*)),
            this, SLOT(onCollectionsFetchJobFinished(KJob*)));

    ChangeRecorder *changeRecorder = new ChangeRecorder( this );
    changeRecorder->setMimeTypeMonitored("text/x-vnd.akonadi.socialfeeditem");
    changeRecorder->setItemFetchScope(scope);

    m_model = new FeedModel(changeRecorder, this);
    m_model->setItemPopulationStrategy(EntityTreeModel::ImmediatePopulation);
    m_model->setCollectionFetchStrategy(EntityTreeModel::InvisibleCollectionFetch);

    m_sortModel = new QSortFilterProxyModel(this);
    m_sortModel->setSourceModel(m_model);
    m_sortModel->setSortRole(FeedModel::StatusTimeRole);
    m_sortModel->setDynamicSortFilter(true);
    m_sortModel->sort(0, Qt::DescendingOrder);
}

SocialFeed::~SocialFeed()
{
}

QSortFilterProxyModel* SocialFeed::model() const
{
    return m_sortModel;
}

QList<QObject*> SocialFeed::collectionsList() const
{
    return m_collectionsList;
}

void SocialFeed::onCollectionsFetched(const Akonadi::Collection::List &list)
{
    SocialNetworkAttributes *attr;
    CollectionQmlItem *item;

    Q_FOREACH(const Akonadi::Collection &collection, list) {
        if (collection.hasAttribute<SocialNetworkAttributes>()) {
            attr = collection.attribute<SocialNetworkAttributes>();
            if (attr->canPublish()) {
                QString collectionTitle = i18nc("String saying 'username on network', it's used in plasmoid tooltip for buttons enabling/disabling "
                                                "particular networks, eg. 'disable posting for bob on facebook'",
                                                "%1 on %2", attr->userName(), attr->networkName());

                if (collection.hasAttribute<EntityDisplayAttribute>()) {
                    item = new CollectionQmlItem(collectionTitle,
                                                 collection.attribute<EntityDisplayAttribute>()->icon(),
                                                 true,
                                                 attr->maxPostLength());
                } else {
                    item = new CollectionQmlItem(collectionTitle, QIcon(), true, attr->maxPostLength());
                }

                item->setCollection(collection);

                m_collectionsList.append(item);
            }
        }
    }

    Q_EMIT collectionsListChanged();
}

void SocialFeed::onCollectionsFetchJobFinished(KJob *job)
{
    if (job->error()) {
        kDebug() << "Error while retrieving collections list:" << job->errorText();
    }
    job->deleteLater();
}

void SocialFeed::onPostMessage(const QString &message)
{
    SocialFeedItem post;
    post.setPostText(message);

    Akonadi::Item item;
    item.setMimeType("text/x-vnd.akonadi.socialfeeditem");
    item.setPayload<SocialFeedItem>(post);

    kDebug() << "Will post to:";

    CollectionQmlItem *collectionItem;
    Q_FOREACH(QObject *object, m_collectionsList) {
        collectionItem = qobject_cast<CollectionQmlItem*>(object);
        if (collectionItem->isChecked()) {
            kDebug() << " *" << collectionItem->name();

            Akonadi::ItemCreateJob *job = new Akonadi::ItemCreateJob(item, collectionItem->collection());
            connect(job, SIGNAL(result(KJob*)),
                    this, SLOT(onItemCreateJobFinished(KJob*)));
            m_postingJobs.append(job);
        }
    }
}

void SocialFeed::onItemCreateJobFinished(KJob *job)
{
    if (job->error()) {
        kDebug() << "Error while creating item:" << job->errorText();
        Q_EMIT statusUpdateError(job->errorString());
    } else {
        kDebug() << "Item created";
    }
    m_postingJobs.removeAll(job);
    if (m_postingJobs.isEmpty()) {
        //only emit the statusUpdated() when all posting jobs have finished
        Q_EMIT statusUpdated();
    }
    job->deleteLater();
}

void SocialFeed::onResfreshCollections()
{
    Q_FOREACH(QObject *collectionObject, m_collectionsList) {
        Akonadi::Collection collection = qobject_cast<CollectionQmlItem*>(collectionObject)->collection();

        if (!testAndSetOnlineResources(collection)) {
            break;
        }

        AgentManager::self()->synchronizeCollection(collection, false);
    }
}

bool SocialFeed::testAndSetOnlineResources(const Collection &collection)
{
    Akonadi::AgentInstance instance = Akonadi::AgentManager::self()->instance(collection.resource());
    if (!instance.isOnline()) {
        if (KMessageBox::questionYesNo(0,
                                      i18n("Before syncing Social Feed, it is necessary to have \"%1\" resource online."
                                            "Do you want to make it online?", collection.name()),
                                       i18n("Resource \"%1\" is offline", instance.name())) != KMessageBox::Yes) {
            return false;
        }
        instance.setIsOnline(true);
    }
    return true;
}

void SocialFeed::onLikePost(const QString &akonadiId, const QString &remoteId, bool liked)
{
    kDebug() << "Liking post" << akonadiId << remoteId << liked;

    Akonadi::ItemFetchScope scope;
    scope.fetchFullPayload();

    Akonadi::Item item(akonadiId);
    item.setRemoteId(remoteId);

    Akonadi::ItemFetchJob *fetchJob = new Akonadi::ItemFetchJob(item);
    fetchJob->setFetchScope(scope);
    fetchJob->setProperty("liked", liked);
    connect(fetchJob, SIGNAL(result(KJob*)), this, SLOT(onItemFetechedForLiking(KJob*)));
}

void SocialFeed::onItemFetechedForLiking(KJob *job)
{
    if (job->error()) {
        kDebug() << job->errorString();
        return;
    }

    Akonadi::ItemFetchJob *fetchJob = qobject_cast<Akonadi::ItemFetchJob*>(job);

    Akonadi::Item item = fetchJob->items().first();

    kDebug() << "Got item!";

    if (!item.hasPayload<SocialFeedItem>()) {
        kDebug() << "Wrong payload!";
        return;
    }

    SocialFeedItem feedItem = item.payload<SocialFeedItem>();
    feedItem.setLiked(fetchJob->property("liked").toBool());
    item.setPayload<SocialFeedItem>(feedItem);

    Akonadi::ItemModifyJob *modifyJob = new Akonadi::ItemModifyJob(item);
    kDebug() << "Modified, running job...";
    connect(modifyJob, SIGNAL(result(KJob*)), this, SLOT(onItemModified(KJob*)));
}

void SocialFeed::onItemModified(KJob *job)
{
    if (job->error()) {
        kDebug() << "Error occurred:" << job->errorString();
    } else {
        kDebug() << "Item modified successfully";
    }
}

void SocialFeed::onShowPost(const QString &akonadiId, const QString &remoteId)
{
    kDebug() << akonadiId << remoteId;
    Akonadi::ItemFetchScope scope;
    scope.fetchPayloadPart("postdetails");

    Akonadi::Item item(akonadiId);
    item.setRemoteId(remoteId);

    Akonadi::ItemFetchJob *fetchJob = new Akonadi::ItemFetchJob(item);
    fetchJob->setFetchScope(scope);
    //connect(fetchJob, SIGNAL(result(KJob*)), this, SLOT(onItemFeteched(KJob*)));
}

int SocialFeed::smallestMaxPostLength() const
{
    int min = 99999;
    Q_FOREACH (QObject *collection, m_collectionsList) {
        int postLength = qobject_cast<CollectionQmlItem*>(collection)->maxPostLength();
        if (postLength < min) {
            min = postLength;
        }
    }

    return min;
}
#include "socialfeed.moc"
