/*
    Social feed model class
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "feedmodel.h"
#include "repliesqmlwrapper.h"

#include <Akonadi/SocialUtils/SocialFeedItem>

#include <KLocalizedString>
#include <KImageCache>
#include <KDateTime>

#include <QImage>

FeedModel::FeedModel(Akonadi::ChangeRecorder* monitor, QObject* parent)
    : EntityTreeModel(monitor, parent)
{
    QHash<int,QByteArray> roles = roleNames();
    roles.insert(ScreenNameRole, QByteArray("screenName"));
    roles.insert(AvatarRole, QByteArray("avatar"));
    roles.insert(NetworkStringRole, QByteArray("networkString"));
    roles.insert(StatusMessageRole, QByteArray("statusMessage"));
    roles.insert(StatusLinkRole, QByteArray("statusLink"));
    roles.insert(StatusLinkTitleRole, QByteArray("statusLinkTitle"));
    roles.insert(StatusImageUrlRole, QByteArray("statusImageUrl"));
    roles.insert(StatusImageRole, QByteArray("statusImage"));
    roles.insert(StatusTimeStringRole, QByteArray("statusTime"));
    roles.insert(StatusInfoRole, QByteArray("statusInfo"));
    roles.insert(StatusLikedRole, QByteArray("statusLike"));
    roles.insert(StatusRepliesRole, QByteArray("statusReplies"));
    roles.insert(StatusIdRole, QByteArray("statusId"));
    roles.insert(AkonadiIdRole, QByteArray("akonadiId"));
    setRoleNames(roles);

    connect(&m_avatarProvider, SIGNAL(imageLoaded(QString,KUrl,QImage)),
            this, SLOT(onImageLoaded(QString,KUrl,QImage)));

//     KImageCache("plasma_engine_preview", 10485760).clear();
}

FeedModel::~FeedModel()
{
    //apparently the plasmoid likes to crash without this...
    m_queuedImages.clear();
}

QVariant FeedModel::data(const QModelIndex &index, int role) const
{
    Akonadi::Item item = EntityTreeModel::data(index, EntityTreeModel::ItemRole).value<Akonadi::Item>();

    if (!item.isValid()) {
        return QVariant();
    }

    if (!item.hasPayload<Akonadi::SocialFeedItem>()) {
        return QVariant();
    }

    QImage image;
    int seconds, minutes, hours, days;
    QString userName;
    QString cacheKey;
    KUrl avatarUrl;

    switch (role) {
        case ScreenNameRole:
            return item.payload<Akonadi::SocialFeedItem>().userDisplayName();
            break;
        case AvatarRole:
            userName = item.payload<Akonadi::SocialFeedItem>().userName();
            avatarUrl = item.payload<Akonadi::SocialFeedItem>().avatarUrl();
            cacheKey = QString::fromUtf8("%1@%2").arg(userName, avatarUrl.pathOrUrl());

            if (avatarUrl.isEmpty() || !avatarUrl.isValid()) {
                //do nothing if we don't have proper url
                return image;
            }

            //check our local cache first
            if (m_imageCache.keys().contains(cacheKey)) {
                return QVariant::fromValue<QImage>(m_imageCache.value(cacheKey));
            } else if (!m_queuedImages.keys().contains(avatarUrl.pathOrUrl())) {
                //if the image is not queued yet, let's load it
                image = m_avatarProvider.loadImage(userName, avatarUrl, true);
                //if the image is not in KImageCache, it returns null image
                if (image.isNull()) {
                    //we are waiting for the image to be loaded by ImageProvider
                    m_queuedImages.insert(avatarUrl.pathOrUrl(), QPersistentModelIndex(index));
                } else {
                    //cache the image locally in the memory
                    m_imageCache.insert(cacheKey, image);
                }
            } else if (!m_queuedImages.values().contains(QPersistentModelIndex(index))) {
                //if the image is queued already and not yet loaded, we end up here,
                //so let's add the new index so we can update all indexes once the image changes
                m_queuedImages.insert(avatarUrl.pathOrUrl(), QPersistentModelIndex(index));
            }
            return image;
            break;
        case NetworkStringRole:
            return QString(item.payload<Akonadi::SocialFeedItem>().networkString());
            break;
        case StatusMessageRole:
            return item.payload<Akonadi::SocialFeedItem>().postText();
            break;
        case StatusLinkRole:
            return item.payload<Akonadi::SocialFeedItem>().postLink();
            break;
        case StatusLinkTitleRole:
            return item.payload<Akonadi::SocialFeedItem>().postLinkTitle();
            break;
        case StatusImageUrlRole:
            return item.payload<Akonadi::SocialFeedItem>().postImageUrl();
            break;
        case StatusImageRole:
            //reuse the variables
            userName = item.payload<Akonadi::SocialFeedItem>().userName();
            avatarUrl = item.payload<Akonadi::SocialFeedItem>().postImageUrl();
            cacheKey = QString::fromUtf8("%1@%2").arg(userName, avatarUrl.pathOrUrl());

            if (avatarUrl.isEmpty() || !avatarUrl.isValid()) {
               return image;
            }

            //check our local cache first
            if (m_imageCache.keys().contains(cacheKey)) {
                return QVariant::fromValue<QImage>(m_imageCache.value(cacheKey));
            } else if (!m_queuedImages.keys().contains(avatarUrl.pathOrUrl())) {
                //if the image is not queued yet, let's load it
                image = m_avatarProvider.loadImage(userName, avatarUrl, false);
                //if the image is not in KImageCache, it returns null image
                if (image.isNull()) {
                    //we are waiting for the image to be loaded by ImageProvider
                    m_queuedImages.insert(avatarUrl.pathOrUrl(), QPersistentModelIndex(index));
                } else {
                    //cache the image locally in the memory
                    m_imageCache.insert(cacheKey, image);
                }
            } else if (!m_queuedImages.values().contains(QPersistentModelIndex(index))) {
                //if the image is queued already and not yet loaded, we end up here,
                //so let's add the new index so we can update all indexes once the image changes
                m_queuedImages.insert(avatarUrl.pathOrUrl(), QPersistentModelIndex(index));
            }
            return image;
            break;
        case StatusTimeRole:
            return item.payload<Akonadi::SocialFeedItem>().postTime().toTime_t();
            break;
        case StatusTimeStringRole:
            seconds = item.payload<Akonadi::SocialFeedItem>().postTime().secsTo(KDateTime::currentDateTime(KDateTime::ClockTime));

            if ( seconds <= 15 ) {
                return i18n( "Just now" );
            }
            if ( seconds <= 45 ) {
                return i18np( "1 sec ago", "%1 secs ago", seconds );
            }

            minutes = ( seconds - 45 + 59 ) / 60;
            if ( minutes <= 45 ) {
                return i18np( "1 min ago", "%1 mins ago", minutes );
            }

            hours = ( seconds - 45 * 60 + 3599 ) / 3600;
            if ( hours <= 18 ) {
                return i18np( "1 hour ago", "%1 hours ago", hours );
            }

            days = ( seconds - 18 * 3600 + 24 * 3600 - 1 ) / ( 24 * 3600 );
            return i18np( "1 day ago", "%1 days ago", days );
            break;
        case StatusInfoRole:
            return item.payload<Akonadi::SocialFeedItem>().postInfo();
        case StatusLikedRole:
            return item.payload<Akonadi::SocialFeedItem>().isLiked();
        case StatusRepliesRole:
            if (m_cachedReplies.contains(item.payload<Akonadi::SocialFeedItem>().postId())) {
                return QVariant::fromValue<QList<QObject*> >(m_cachedReplies.value(item.payload<Akonadi::SocialFeedItem>().postId()));
            } else {
                QList<QObject*> repliesList = wrapRepliesForQML(item.payload<Akonadi::SocialFeedItem>().postReplies(),
                                                                item.payload<Akonadi::SocialFeedItem>().postId());
                m_cachedReplies.insert(item.payload<Akonadi::SocialFeedItem>().postId(), repliesList);
                return QVariant::fromValue<QList<QObject*> >(repliesList);
            }
        case StatusIdRole:
            return item.payload<Akonadi::SocialFeedItem>().postId();
        case AkonadiIdRole:
            return item.id();
        default:
            return QVariant();
    }


    return EntityTreeModel::entityData(item, 0, role);
}

QVariant FeedModel::entityData (const Akonadi::Collection& collection, int column, int role) const
{
    return Akonadi::EntityTreeModel::entityData (collection, column, role);
}

int FeedModel::entityColumnCount (Akonadi::EntityTreeModel::HeaderGroup headerGroup) const
{
    return 1;
}

void FeedModel::onImageLoaded(const QString &who, const KUrl &url, const QImage &image)
{
    Q_FOREACH(const QPersistentModelIndex &index, m_queuedImages.values(url.pathOrUrl())) {
        m_imageCache.insert(QString::fromUtf8("%1@%2").arg(who, url.pathOrUrl()), image);
        emit dataChanged(index, index);
    }

    m_queuedImages.remove(url.pathOrUrl());
}

QList<QObject*> FeedModel::wrapRepliesForQML(QList<Akonadi::SocialFeedItem> postReplies, const QString &postId) const
{
    QList<QObject*> repliesList;
    Q_FOREACH(const Akonadi::SocialFeedItem &postReply, postReplies) {
        repliesList.append(new RepliesQMLWrapper(postReply, postId));
    }

    return repliesList;
}
