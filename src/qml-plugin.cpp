/*
    Copyright (C) 2011  Lasath Fernando <kde@lasath.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "qml-plugin.h"

#include <QtDeclarative/QDeclarativeItem>

#include "socialfeed.h"

void QmlPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<SocialFeed> (uri, 0, 1, "SocialFeed");
}

Q_EXPORT_PLUGIN2(socialfeedqmlplugin, QmlPlugin);
