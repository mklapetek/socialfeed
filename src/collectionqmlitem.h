/*
    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef COLLECTIONQMLITEM_H
#define COLLECTIONQMLITEM_H

#include <QObject>
#include <QString>
#include <QIcon>

#include <Akonadi/Collection>

class CollectionQmlItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name);
    Q_PROPERTY(QIcon icon READ icon);
    Q_PROPERTY(bool checked READ isChecked WRITE setChecked);
    Q_PROPERTY(uint maxPostLength READ maxPostLength);

public:
    CollectionQmlItem(const QString &name, const QIcon &icon, bool checked = true, uint maxPostLength = 140, QObject *parent = 0);
    ~CollectionQmlItem();

    QString name() const;
    void setName(const QString &name);

    QIcon icon() const;
    void setIcon(const QIcon &icon);

    bool isChecked() const;
    void setChecked(bool checked);

    uint maxPostLength() const;
    void setMaxPostLength(uint maxPostLength);

    Akonadi::Collection collection() const;
    void setCollection(const Akonadi::Collection &collection);

private:
    QString m_name;
    QIcon m_icon;
    bool m_checked;
    uint m_maxPostLength;
    Akonadi::Collection m_collection;
};

#endif // COLLECTIONQMLITEM_H
