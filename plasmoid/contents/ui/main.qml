/*
 *  Social feed testing plasmoid
 *  Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

import Qt 4.7

import org.kde.socialfeed 0.1 as SocialFeedObject
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.qtextracomponents 0.1 as Extra

Item {
    id: main;

    width: 400
    height: 300


    SocialFeedObject.SocialFeed {
        id: socialFeed
    }

    // contact listview
    ListView {
        id: socialFeedList;
        focus: true
        cacheBuffer: 260

        anchors {
            top: parent.top;
            topMargin: 5;
            left: parent.left;
            right: parent.right;
            rightMargin: 1
            bottom: parent.bottom;
        }

        clip: true;
        model: socialFeed.model;
        boundsBehavior: Flickable.StopAtBounds

        delegate: Item {
            height: textColumn.height + 16
            width: parent.width

            Extra.QImageItem
            {
                id: avatarImage
                width: 48
                height: 48
                anchors {
                    left: parent.left
                    top: parent.top
                    leftMargin: 2
                    topMargin: 2
                    bottomMargin: 8
                }
                image: model.avatar
            }

        Column {
            id: textColumn;
            width: parent.width

            anchors {
                left: avatarImage.right
                right: parent.right
                leftMargin: 8
                bottomMargin: 8
            }
            Text {
                id: screenNameText
                text: model.screenName + " " + model.networkString + ":"
                font.bold: true
            }
            Text {
                id: statusMessageText
                text: model.statusMessage
                wrapMode: Text.WordWrap
                width: parent.width
                visible: statusMessageText.text.length == 0 ? false : true
            }
            Rectangle {
                id: linkRect
                border.width: 1
                border.color: "#BBBBBB"
                height: 100
                width: 240
                visible: statusLinkTitleText.text.length == 0 ? false : true

                Extra.QImageItem
                {
                    id: statusImageItem
                    width: statusImageItem.null ? 0 : 100
                    height: 100
                    fillMode: Image.PreserveAspectCrop

                    anchors {
                        top: linkRect.top
                        bottom: linkRect.bottom
                        left: linkRect.left
                        topMargin: 1
                        leftMargin: 1
                    }
                    image: model.statusImage
                }

                Text {
                    id: statusLinkTitleText
                    anchors {
                        topMargin: 8
                        leftMargin: 4
                        rightMargin: 4
                        top: linkRect.top
                        left: statusImageItem.right
                        right: linkRect.right
//                         bottom: statusImageItem.null ? linkRect.bottom
                    }
                    font.bold: true
                    text: model.statusLinkTitle.length == 0 ? "" : "<a href=\"" + model.statusLink + "\">" + model.statusLinkTitle + "</a>"
                    wrapMode: Text.Wrap
                    visible: statusLinkTitleText.text.length == 0 ? false : true
                }
                Text {
                    id: statusLink
                    anchors {
                        top: statusLinkTitleText.bottom
                        left: statusImageItem.right
                        right: linkRect.right
                        bottom: linkRect.bottom
                    }
                    text: "<a href=\"" + model.statusLink + "\">" + model.statusLink + "</a>"
                    wrapMode: Text.Wrap
                    visible: statusImageItem.null
                }

            }
            Text {
                text: model.statusTime + (model.statusInfo.length == 0 ? "" : " | " + model.statusInfo)
                color: "#777777"
            }
        }

//         highlight: highlightBar
//         highlightMoveDuration: 250
//         highlightMoveSpeed: 1
//         highlightFollowsCurrentItem: true
        }
    }

    Component.onCompleted: {
        plasmoid.aspectRatioMode = IgnoreAspectRatio;
    }
}



